import { FliFrontK3mPage } from './app.po';

describe('fli-front-k3m App', () => {
  let page: FliFrontK3mPage;

  beforeEach(() => {
    page = new FliFrontK3mPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
