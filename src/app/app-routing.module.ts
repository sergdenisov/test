import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import { FlicksListComponent } from './flicks-list/flicks-list.component';
import { FlicksGalleryComponent } from './flicks-gallery/flicks-gallery.component';
import {FlickFullComponent} from './flick-full/flick-full.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/list',
    pathMatch: 'full'
  },
  {
    path: 'list',
    component: FlicksListComponent
  },
  {
    path: 'gallery',
    component: FlicksGalleryComponent
  }
  ,
  {
    path: 'full/:index/:source',
    component: FlickFullComponent,
  }
]

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
