import { TestBed, inject } from '@angular/core/testing';

import { FlicksService } from './flicks.service';

describe('FlicksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FlicksService]
    });
  });

  it('should be created', inject([FlicksService], (service: FlicksService) => {
    expect(service).toBeTruthy();
  }));
});
