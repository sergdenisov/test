export class Flick {
  author: string;
  imageName: string;
  imageUrl: string;

  constructor(author: string, media: string, title: string) {
    this.author = author;
    this.imageName = title;
    this.imageUrl = media['m'];
  }
}
