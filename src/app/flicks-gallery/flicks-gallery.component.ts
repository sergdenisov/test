import { Component, OnInit } from '@angular/core';
import {Flick} from "../flick";
import {FlicksService} from "../flicks.service";
import {LoggerService} from "../utils/logger.service";
import {Router} from "@angular/router";
import {logger} from "codelyzer/util/logger";

@Component({
  selector: 'app-flicks-gallery',
  templateUrl: './flicks-gallery.component.html',
  styleUrls: ['./flicks-gallery.component.css']
})
export class FlicksGalleryComponent implements OnInit {

  flicks: Flick[];
  cols: number[];
  rows: number[];

  constructor(public flicksService: FlicksService, private router: Router, private logger:LoggerService) {}

  ngOnInit() {
    //this.getFlicks('');
  }

  arrange()
  {
    let cnt:number = this.flicks.length;

    let columnsCount = 4;
    let rowCount = cnt / columnsCount;
    //+ (cnt%this.columnsCount === 0 ? 0 : 1)
    this.rows = [];
    this.cols = [];
    this.cols.length = columnsCount;
    for(let i=0;i<rowCount;i++)
      this.rows.push(i);
  }

  onFlickClicked(flickIndex: number) {
    this.logger.log('Gallery onFlickClicked - index ', flickIndex);
    this.router.navigate(['/full', flickIndex, 'gallery']);
  }

  getFlicks(tags: String) {
    // this.flicksService.getFlicks('').then(flicks => {
    //   this.flicks = this.flicksService.flicks;
    //   this.logger.log('Gallery - flicks got:', flicks);
    //   this.arrange();
    // });
  }
}
