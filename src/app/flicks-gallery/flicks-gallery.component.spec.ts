import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlicksGalleryComponent } from './flicks-gallery.component';

describe('FlicksGalleryComponent', () => {
  let component: FlicksGalleryComponent;
  let fixture: ComponentFixture<FlicksGalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlicksGalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlicksGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
