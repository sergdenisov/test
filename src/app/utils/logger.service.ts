import {Injectable} from '@angular/core';

@Injectable()
export class LoggerService {

  log(...data: any[]): void {
    console.log.apply(this, data);
  }

  error(...data: any[]): void {
    console.error.apply(this, data);
  }
}
