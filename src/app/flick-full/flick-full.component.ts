import { Component, OnInit } from '@angular/core';
import {FlicksService} from "../flicks.service";
import {LoggerService} from "../utils/logger.service";
import {ActivatedRoute, Params} from '@angular/router';
import {Location} from '@angular/common';
import 'rxjs/add/operator/switchMap';
import {Flick} from "../flick";

@Component({
  selector: 'app-flick-full',
  templateUrl: './flick-full.component.html',
  styleUrls: ['./flick-full.component.css']
})
export class FlickFullComponent implements OnInit {
  private flicks:Flick[];
  private flick:Flick;
  private index:number;

  constructor(private flicksService: FlicksService, private route: ActivatedRoute, private location: Location, private logger: LoggerService) { }

  ngOnInit() {
    let self = this;
    this.route.params.subscribe((params: Params) => {
      self.index = +params['index'];
      self.flicks = self.flicksService.flicks

      if(self.flicks) {
        if (self.index > self.flicks.length) self.index = 0;
        self.flick = self.flicks[self.index];
      }

      self.logger.log('FlickFullComponent: params ', self.index, self.flicks);
    });

    //variant from service
    // this.route.params.switchMap((params: Params) => {
    //   return this.flicksService.getFlick(+params['index'])
    // })
    //   .subscribe(flick => {
    //     this.logger.log('FlickFullComponent: flick ', flick);
    //     this.flick = flick;
    //   });
  }

  goBack() {
    this.location.back();
  }

  goNext() {
    if(this.index<this.flicks.length) this.flick = this.flicks[++this.index];
  }

  goPrev() {
    if(this.index>0) this.flick = this.flicks[--this.index];
  }
}
