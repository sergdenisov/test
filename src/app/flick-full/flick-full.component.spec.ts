import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlickFullComponent } from './flick-full.component';

describe('FlickFullComponent', () => {
  let component: FlickFullComponent;
  let fixture: ComponentFixture<FlickFullComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlickFullComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlickFullComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
