import { Injectable } from '@angular/core';
import {LoggerService} from './utils/logger.service';
import {Flick} from './flick';
import {Http} from "@angular/http";
import {Jsonp} from '@angular/http';
import {logger} from "codelyzer/util/logger";
import "rxjs/add/operator/toPromise";
import "rxjs/add/operator/map";

import { Observable }        from 'rxjs/Observable';
import { Subject }           from 'rxjs/Subject';
// Observable class extensions
import 'rxjs/add/observable/of';
// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';


//noinspection TsLint
const flicks = [
    {
      "title": "IMG_8614",
      "link": "https:\/\/www.flickr.com\/photos\/storvandre\/33999296374\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4196\/33999296374_15d485edb5_m.jpg"},
      "date_taken": "2016-12-08T17:26:20-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/storvandre\/\">storvandre<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/storvandre\/33999296374\/\" title=\"IMG_8614\"><img src=\"https:\/\/farm5.staticflickr.com\/4196\/33999296374_15d485edb5_m.jpg\" width=\"240\" height=\"160\" alt=\"IMG_8614\" \/><\/a><\/p> <p>08.12.2016 \/\/ Conversano<\/p>",
      "published": "2017-05-23T11:42:37Z",
      "author": "nobody@flickr.com (\"storvandre\")",
      "author_id": "40695821@N00",
      "tags": "storvandre conversano bari puglia apulia rural countryside nature landscape"
    },
    {
      "title": "20170402_152314",
      "link": "https:\/\/www.flickr.com\/photos\/146892274@N07\/33999297314\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4249\/33999297314_e26a7f35dd_m.jpg"},
      "date_taken": "2017-04-02T15:23:14-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/146892274@N07\/\">javiermorales13<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/146892274@N07\/33999297314\/\" title=\"20170402_152314\"><img src=\"https:\/\/farm5.staticflickr.com\/4249\/33999297314_e26a7f35dd_m.jpg\" width=\"240\" height=\"180\" alt=\"20170402_152314\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:42Z",
      "author": "nobody@flickr.com (\"javiermorales13\")",
      "author_id": "146892274@N07",
      "tags": ""
    },
    {
      "title": " ",
      "link": "https:\/\/www.flickr.com\/photos\/147463993@N04\/33999297834\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4251\/33999297834_4917f98c9e_m.jpg"},
      "date_taken": "2017-05-22T19:31:55-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/147463993@N04\/\">smallblock98<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/147463993@N04\/33999297834\/\" title=\" \"><img src=\"https:\/\/farm5.staticflickr.com\/4251\/33999297834_4917f98c9e_m.jpg\" width=\"240\" height=\"180\" alt=\" \" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:44Z",
      "author": "nobody@flickr.com (\"smallblock98\")",
      "author_id": "147463993@N04",
      "tags": ""
    },
    {
      "title": "2017-05-13 03-48-53 - DSC01150",
      "link": "https:\/\/www.flickr.com\/photos\/128492816@N03\/34032154153\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4220\/34032154153_8764d72eb0_m.jpg"},
      "date_taken": "2017-05-13T03:48:53-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/128492816@N03\/\">h0ckeypuck16<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/128492816@N03\/34032154153\/\" title=\"2017-05-13 03-48-53 - DSC01150\"><img src=\"https:\/\/farm5.staticflickr.com\/4220\/34032154153_8764d72eb0_m.jpg\" width=\"160\" height=\"240\" alt=\"2017-05-13 03-48-53 - DSC01150\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:41Z",
      "author": "nobody@flickr.com (\"h0ckeypuck16\")",
      "author_id": "128492816@N03",
      "tags": ""
    },
    {
      "title": "Khuram shehzad butt",
      "link": "https:\/\/www.flickr.com\/photos\/80240306@N06\/34455806120\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4199\/34455806120_6e3b027a03_m.jpg"},
      "date_taken": "2016-06-28T08:58:57-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/80240306@N06\/\">khuram butt<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/80240306@N06\/34455806120\/\" title=\"Khuram shehzad butt\"><img src=\"https:\/\/farm5.staticflickr.com\/4199\/34455806120_6e3b027a03_m.jpg\" width=\"135\" height=\"240\" alt=\"Khuram shehzad butt\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:47Z",
      "author": "nobody@flickr.com (\"khuram butt\")",
      "author_id": "80240306@N06",
      "tags": ""
    },
    {
      "title": "020715_042",
      "link": "https:\/\/www.flickr.com\/photos\/154751686@N07\/34455806410\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4268\/34455806410_0b298e8aff_m.jpg"},
      "date_taken": "2002-07-15T17:26:37-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/154751686@N07\/\">hartmutmodes<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/154751686@N07\/34455806410\/\" title=\"020715_042\"><img src=\"https:\/\/farm5.staticflickr.com\/4268\/34455806410_0b298e8aff_m.jpg\" width=\"240\" height=\"180\" alt=\"020715_042\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:49Z",
      "author": "nobody@flickr.com (\"hartmutmodes\")",
      "author_id": "154751686@N07",
      "tags": ""
    },
    {
      "title": "MFS084Sugar35.jpg",
      "link": "https:\/\/www.flickr.com\/photos\/xiuren\/34678807842\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4276\/34678807842_02745a241e_m.jpg"},
      "date_taken": "2016-10-29T14:48:47-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/xiuren\/\">CHINA\u65f6\u5c1a\u6027\u611f\u79c0\u4eba\u6a21\u7279<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/xiuren\/34678807842\/\" title=\"MFS084Sugar35.jpg\"><img src=\"https:\/\/farm5.staticflickr.com\/4276\/34678807842_02745a241e_m.jpg\" width=\"164\" height=\"240\" alt=\"MFS084Sugar35.jpg\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:40Z",
      "author": "nobody@flickr.com (\"CHINA\\u65f6\\u5c1a\\u6027\\u611f\\u79c0\\u4eba\\u6a21\\u7279\")",
      "author_id": "148908351@N05",
      "tags": ""
    },
    {
      "title": "Judith Regan Net Worth",
      "link": "https:\/\/www.flickr.com\/photos\/152217530@N03\/34678808242\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4276\/34678808242_f9215bc04e_m.jpg"},
      "date_taken": "2017-05-23T04:42:41-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/152217530@N03\/\">HowRichest<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/152217530@N03\/34678808242\/\" title=\"Judith Regan Net Worth\"><img src=\"https:\/\/farm5.staticflickr.com\/4276\/34678808242_f9215bc04e_m.jpg\" width=\"204\" height=\"204\" alt=\"Judith Regan Net Worth\" \/><\/a><\/p> <p><a href=\"https:\/\/www.howrichest.com\/wp-content\/uploads\/2017\/05\/Judith-Regan-Net-Worth.jpg\" rel=\"nofollow\">www.howrichest.com\/wp-content\/uploads\/2017\/05\/Judith-Rega...<\/a><br \/> <a href=\"https:\/\/www.howrichest.com\/judith-regan-net-worth\/\" rel=\"nofollow\">www.howrichest.com\/judith-regan-net-worth\/<\/a><\/p>",
      "published": "2017-05-23T11:42:41Z",
      "author": "nobody@flickr.com (\"HowRichest\")",
      "author_id": "152217530@N03",
      "tags": ""
    },
    {
      "title": "P1050051",
      "link": "https:\/\/www.flickr.com\/photos\/114217466@N08\/34678808672\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4189\/34678808672_b0984f2d50_m.jpg"},
      "date_taken": "2017-05-16T01:18:50-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/114217466@N08\/\">wefarmer<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/114217466@N08\/34678808672\/\" title=\"P1050051\"><img src=\"https:\/\/farm5.staticflickr.com\/4189\/34678808672_b0984f2d50_m.jpg\" width=\"240\" height=\"160\" alt=\"P1050051\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:43Z",
      "author": "nobody@flickr.com (\"wefarmer\")",
      "author_id": "114217466@N08",
      "tags": ""
    },
    {
      "title": "Niver Daniel e visita Tuiuti",
      "link": "https:\/\/www.flickr.com\/photos\/150940923@N05\/34678810212\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4227\/34678810212_ae251ed4a0_m.jpg"},
      "date_taken": "2017-02-19T18:01:48-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/150940923@N05\/\">mariajunqueira1<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/150940923@N05\/34678810212\/\" title=\"Niver Daniel e visita Tuiuti\"><img src=\"https:\/\/farm5.staticflickr.com\/4227\/34678810212_ae251ed4a0_m.jpg\" width=\"180\" height=\"240\" alt=\"Niver Daniel e visita Tuiuti\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:48Z",
      "author": "nobody@flickr.com (\"mariajunqueira1\")",
      "author_id": "150940923@N05",
      "tags": ""
    },
    {
      "title": "Gh",
      "link": "https:\/\/www.flickr.com\/photos\/150948853@N05\/34678810672\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4201\/34678810672_0b7dbd75f0_m.jpg"},
      "date_taken": "2017-05-23T04:42:49-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/150948853@N05\/\">hamadali6<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/150948853@N05\/34678810672\/\" title=\"Gh\"><img src=\"https:\/\/farm5.staticflickr.com\/4201\/34678810672_0b7dbd75f0_m.jpg\" width=\"240\" height=\"180\" alt=\"Gh\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:49Z",
      "author": "nobody@flickr.com (\"hamadali6\")",
      "author_id": "150948853@N05",
      "tags": ""
    },
    {
      "title": "\u3042",
      "link": "https:\/\/www.flickr.com\/photos\/154957078@N08\/34678810982\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4195\/34678810982_bec6a9db78_m.jpg"},
      "date_taken": "2017-05-23T04:42:50-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/154957078@N08\/\">mayayamazaki<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/154957078@N08\/34678810982\/\" title=\"\u3042\"><img src=\"https:\/\/farm5.staticflickr.com\/4195\/34678810982_bec6a9db78_m.jpg\" width=\"135\" height=\"240\" alt=\"\u3042\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:50Z",
      "author": "nobody@flickr.com (\"mayayamazaki\")",
      "author_id": "154957078@N08",
      "tags": ""
    },
    {
      "title": "2017-05-23 at 05-27-09",
      "link": "https:\/\/www.flickr.com\/photos\/pahowho\/34709772611\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4249\/34709772611_a5dd398268_m.jpg"},
      "date_taken": "2017-05-23T05:27:09-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/pahowho\/\">Pan American Health Organization PAHO<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/pahowho\/34709772611\/\" title=\"2017-05-23 at 05-27-09\"><img src=\"https:\/\/farm5.staticflickr.com\/4249\/34709772611_a5dd398268_m.jpg\" width=\"240\" height=\"160\" alt=\"2017-05-23 at 05-27-09\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:46Z",
      "author": "nobody@flickr.com (\"Pan American Health Organization PAHO\")",
      "author_id": "87642443@N05",
      "tags": ""
    },
    {
      "title": "Puy du Fou du 8 au 13 mai",
      "link": "https:\/\/www.flickr.com\/photos\/155136019@N03\/34802613706\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4269\/34802613706_8bb69011b1_m.jpg"},
      "date_taken": "2017-01-30T10:55:30-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155136019@N03\/\">francinewery1<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155136019@N03\/34802613706\/\" title=\"Puy du Fou du 8 au 13 mai\"><img src=\"https:\/\/farm5.staticflickr.com\/4269\/34802613706_8bb69011b1_m.jpg\" width=\"240\" height=\"135\" alt=\"Puy du Fou du 8 au 13 mai\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:47Z",
      "author": "nobody@flickr.com (\"francinewery1\")",
      "author_id": "155136019@N03",
      "tags": ""
    },
    {
      "title": "\u0412 \u043d\u0430\u043b\u0438\u0447\u0438\u0438 \u043b\u0435\u0442\u043d\u0435\u0435 \u043f\u043b\u0430\u0442\u044c\u0435 \u0441 \u043a\u0440\u0443\u0436\u0435\u0432\u043e\u043c\ud83d\ude0d \u0440\u0430\u0437\u043c\u0435\u0440 \u0435\u0434\u0438\u043d\u044b\u0439 (\u043e\u0442 42 \u0434\u043e 46), 1700\u20bd \u2705\u0410\u043a\u0442\u0443\u0430\u043b\u044c\u043d\u043e\u0435 \u043d\u0430\u043b\u0438\u0447\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u043e\u0432 \u0441\u043c\u043e\u0442\u0440\u0438\u0442\u0435 \u0432 \u0432\u043a vk.com\/show_room_sss \u2705\u041d\u043e\u0432\u0438\u043d\u043a\u0438 2 \u0440\u0430\u0437\u0430 \u0432 \u043d\u0435\u0434\u0435\u043b\u044e! \u2705\u0414\u043e\u0441\u0442\u0430\u0432\u043a\u0430 \u043a\u0443\u0440\u044c\u0435\u0440\u043e\u043c 350\u20bd :white_c",
      "link": "https:\/\/www.flickr.com\/photos\/152386364@N02\/34802613936\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4267\/34802613936_4ca1395a85_m.jpg"},
      "date_taken": "2017-05-23T04:42:49-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/152386364@N02\/\">Show room Svetlana Seredenko<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/152386364@N02\/34802613936\/\" title=\"\u0412 \u043d\u0430\u043b\u0438\u0447\u0438\u0438 \u043b\u0435\u0442\u043d\u0435\u0435 \u043f\u043b\u0430\u0442\u044c\u0435 \u0441 \u043a\u0440\u0443\u0436\u0435\u0432\u043e\u043c\ud83d\ude0d \u0440\u0430\u0437\u043c\u0435\u0440 \u0435\u0434\u0438\u043d\u044b\u0439 (\u043e\u0442 42 \u0434\u043e 46), 1700\u20bd \u2705\u0410\u043a\u0442\u0443\u0430\u043b\u044c\u043d\u043e\u0435 \u043d\u0430\u043b\u0438\u0447\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u043e\u0432 \u0441\u043c\u043e\u0442\u0440\u0438\u0442\u0435 \u0432 \u0432\u043a vk.com\/show_room_sss \u2705\u041d\u043e\u0432\u0438\u043d\u043a\u0438 2 \u0440\u0430\u0437\u0430 \u0432 \u043d\u0435\u0434\u0435\u043b\u044e! \u2705\u0414\u043e\u0441\u0442\u0430\u0432\u043a\u0430 \u043a\u0443\u0440\u044c\u0435\u0440\u043e\u043c 350\u20bd :white_c\"><img src=\"https:\/\/farm5.staticflickr.com\/4267\/34802613936_4ca1395a85_m.jpg\" width=\"240\" height=\"240\" alt=\"\u0412 \u043d\u0430\u043b\u0438\u0447\u0438\u0438 \u043b\u0435\u0442\u043d\u0435\u0435 \u043f\u043b\u0430\u0442\u044c\u0435 \u0441 \u043a\u0440\u0443\u0436\u0435\u0432\u043e\u043c\ud83d\ude0d \u0440\u0430\u0437\u043c\u0435\u0440 \u0435\u0434\u0438\u043d\u044b\u0439 (\u043e\u0442 42 \u0434\u043e 46), 1700\u20bd \u2705\u0410\u043a\u0442\u0443\u0430\u043b\u044c\u043d\u043e\u0435 \u043d\u0430\u043b\u0438\u0447\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u043e\u0432 \u0441\u043c\u043e\u0442\u0440\u0438\u0442\u0435 \u0432 \u0432\u043a vk.com\/show_room_sss \u2705\u041d\u043e\u0432\u0438\u043d\u043a\u0438 2 \u0440\u0430\u0437\u0430 \u0432 \u043d\u0435\u0434\u0435\u043b\u044e! \u2705\u0414\u043e\u0441\u0442\u0430\u0432\u043a\u0430 \u043a\u0443\u0440\u044c\u0435\u0440\u043e\u043c 350\u20bd :white_c\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:49Z",
      "author": "nobody@flickr.com (\"Show room Svetlana Seredenko\")",
      "author_id": "152386364@N02",
      "tags": "instagramapp square squareformat iphoneography uploaded:by=instagram"
    },
    {
      "title": "La BALME de SILLINGY VINTAGE PARTY 7 21\/05\/2017 TRIUMPH TR3",
      "link": "https:\/\/www.flickr.com\/photos\/128320812@N08\/34841947975\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4243\/34841947975_a2879f8a1a_m.jpg"},
      "date_taken": "2017-05-21T17:53:45-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/128320812@N08\/\">stef81cross<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/128320812@N08\/34841947975\/\" title=\"La BALME de SILLINGY VINTAGE PARTY 7 21\/05\/2017 TRIUMPH TR3\"><img src=\"https:\/\/farm5.staticflickr.com\/4243\/34841947975_a2879f8a1a_m.jpg\" width=\"240\" height=\"180\" alt=\"La BALME de SILLINGY VINTAGE PARTY 7 21\/05\/2017 TRIUMPH TR3\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:39Z",
      "author": "nobody@flickr.com (\"stef81cross\")",
      "author_id": "128320812@N08",
      "tags": ""
    },
    {
      "title": "Gomen\u00e9_2017_21-mai-_17-54__1965.jpg",
      "link": "https:\/\/www.flickr.com\/photos\/132184599@N06\/34841948515\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4275\/34841948515_9a100418ea_m.jpg"},
      "date_taken": "2017-05-21T17:54:24-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/132184599@N06\/\">Breizh Cross Tour<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/132184599@N06\/34841948515\/\" title=\"Gomen\u00e9_2017_21-mai-_17-54__1965.jpg\"><img src=\"https:\/\/farm5.staticflickr.com\/4275\/34841948515_9a100418ea_m.jpg\" width=\"240\" height=\"160\" alt=\"Gomen\u00e9_2017_21-mai-_17-54__1965.jpg\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:41Z",
      "author": "nobody@flickr.com (\"Breizh Cross Tour\")",
      "author_id": "132184599@N06",
      "tags": "2017 gomen\u00e9 gomene gom\u00e9n\u00e9"
    },
    {
      "title": "Black eggs have pink shells #blackegg #preservedegg #PhnomPenh #Cambodia",
      "link": "https:\/\/www.flickr.com\/photos\/seattleraindrops\/34841948545\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4274\/34841948545_8b92154b78_m.jpg"},
      "date_taken": "2017-05-23T04:42:41-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/seattleraindrops\/\">Seattle Raindrops<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/seattleraindrops\/34841948545\/\" title=\"Black eggs have pink shells #blackegg #preservedegg #PhnomPenh #Cambodia\"><img src=\"https:\/\/farm5.staticflickr.com\/4274\/34841948545_8b92154b78_m.jpg\" width=\"240\" height=\"240\" alt=\"Black eggs have pink shells #blackegg #preservedegg #PhnomPenh #Cambodia\" \/><\/a><\/p> <p>via Instagram <a href=\"http:\/\/ift.tt\/2rOGtVK\" rel=\"nofollow\">ift.tt\/2rOGtVK<\/a><\/p>",
      "published": "2017-05-23T11:42:41Z",
      "author": "nobody@flickr.com (\"Seattle Raindrops\")",
      "author_id": "7239376@N05",
      "tags": "ifttt instagram"
    },
    {
      "title": "4I2A0464.jpg",
      "link": "https:\/\/www.flickr.com\/photos\/129293620@N05\/34841950495\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4204\/34841950495_78c76027f0_m.jpg"},
      "date_taken": "2017-04-15T09:32:16-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/129293620@N05\/\">Henry Kirkwood<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/129293620@N05\/34841950495\/\" title=\"4I2A0464.jpg\"><img src=\"https:\/\/farm5.staticflickr.com\/4204\/34841950495_78c76027f0_m.jpg\" width=\"240\" height=\"135\" alt=\"4I2A0464.jpg\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:46Z",
      "author": "nobody@flickr.com (\"Henry Kirkwood\")",
      "author_id": "129293620@N05",
      "tags": ""
    },
    {
      "title": "\u7b51\u6ce2\u5b9f\u9a13\u690d\u7269\u5712 \u30b7\u30b0\u3076\u3089\u30dc\u30c3\u30c1\uff01 (*\u00b4 \u8278\uff40) Simply not just simple. \u30fe(*\u00b4\u2200\uff40*)\uff89 by sd Quattro H #Sigma #globalcapture #ig_allaroundyou_invite #ig_eurasia #Japan #we_are_explorers #\u6771\u4eac\u30ab\u30e1\u30e9\u90e8 #tokyocameraclub #ig_captures #AVContest #sdq_h#sd_Quattro_h #instagramJapan #ig_mast",
      "link": "https:\/\/www.flickr.com\/photos\/144576835@N03\/34841950775\/",
      "media": {"m":"https:\/\/farm5.staticflickr.com\/4204\/34841950775_4c8ac02204_m.jpg"},
      "date_taken": "2017-05-23T04:42:47-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/144576835@N03\/\">clacknoty-us<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/144576835@N03\/34841950775\/\" title=\"\u7b51\u6ce2\u5b9f\u9a13\u690d\u7269\u5712 \u30b7\u30b0\u3076\u3089\u30dc\u30c3\u30c1\uff01 (*\u00b4 \u8278\uff40) Simply not just simple. \u30fe(*\u00b4\u2200\uff40*)\uff89 by sd Quattro H #Sigma #globalcapture #ig_allaroundyou_invite #ig_eurasia #Japan #we_are_explorers #\u6771\u4eac\u30ab\u30e1\u30e9\u90e8 #tokyocameraclub #ig_captures #AVContest #sdq_h#sd_Quattro_h #instagramJapan #ig_mast\"><img src=\"https:\/\/farm5.staticflickr.com\/4204\/34841950775_4c8ac02204_m.jpg\" width=\"240\" height=\"160\" alt=\"\u7b51\u6ce2\u5b9f\u9a13\u690d\u7269\u5712 \u30b7\u30b0\u3076\u3089\u30dc\u30c3\u30c1\uff01 (*\u00b4 \u8278\uff40) Simply not just simple. \u30fe(*\u00b4\u2200\uff40*)\uff89 by sd Quattro H #Sigma #globalcapture #ig_allaroundyou_invite #ig_eurasia #Japan #we_are_explorers #\u6771\u4eac\u30ab\u30e1\u30e9\u90e8 #tokyocameraclub #ig_captures #AVContest #sdq_h#sd_Quattro_h #instagramJapan #ig_mast\" \/><\/a><\/p> ",
      "published": "2017-05-23T11:42:47Z",
      "author": "nobody@flickr.com (\"clacknoty-us\")",
      "author_id": "144576835@N03",
      "tags": "instagramapp square squareformat iphoneography uploaded:by=instagram"
    }
  ];

@Injectable()
export class FlicksService {
  private url:string = 'https://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=JSONP_CALLBACK&format=json';

  public flicks:any[];
  public cols:number[];
  public rows:number[];

  private tags = new Subject<string>();

  public taggedFlicks: Observable<Flick[]> = this.tags
    .debounceTime(300)
    .distinctUntilChanged()
    .switchMap(tags => {
      return this.searchFlicks(tags);

      // return tags ?
      //   //Observable.of<Flick[]>([new Flick('A', 'T', 'L')]) :
      //   this.searchFlicks(tags) :
      //   Observable.of<Flick[]>([])
    })
    .catch(error => {
      console.log(error);
      return Observable.of<Flick[]>([]);
    });


  constructor(private http: Http, private logger: LoggerService, private jsonp: Jsonp) { }

  rearrangeColsRows()
  {
    let cnt:number = this.flicks.length;

    let columnsCount = 4;
    let rowCount = cnt / columnsCount;
    //+ (cnt%this.columnsCount === 0 ? 0 : 1)
    this.rows = [];
    this.cols = [];
    this.cols.length = columnsCount;
    for(let i=0;i<rowCount;i++)
      this.rows.push(i);
  }

  updateTags(tags: string, force:boolean)
  {
    this.logger.log('updateTags calledd:', tags);
    if(force) this.tags.next(tags+'_');
    this.tags.next(tags);
  }

  searchFlicks(tags: string): Observable<Flick[]> {
    var self = this;

    let url = self.url;
    if(tags) {
      url += '&tags=' + tags;
    }
    return this.jsonp.get(url).map((response: any) => {
      self.flicks = response.json().items;
      self.rearrangeColsRows();
      self.logger.log('searchFlicks response got:', self.flicks, self.rows);
      return response.json().items as Flick[];
    });
  }

  getFlicks(tags: string): Promise<Flick[]> {
    var self = this;

    let url = self.url;
    if(tags) {
      url += '&tags=' + tags;
    }

    return this.jsonp.get(url).map((response: any) => response.json()).toPromise().then(
      (response) => {
        self.logger.log('getFlicks response got:', response.items);
        self.flicks = response.items;
        self.rearrangeColsRows();

        //setTimeout(() => {self.getFlicks('');}, 3000);

        return response.items;
      }
    );

    // return this.http.get(this.url)
    //   .toPromise()
    //   .then(response => {
    //       let data = response.json().data;
    //       self.logger.log('Data got:', data);
    //
    //       self.flicks = flicks;
    //       return self.flicks;
    //     }
    //     )
    //   .catch(self.handleError);

    // return new Promise(resolve => {setTimeout(() => {
    //   this.flicks = flicks;
    //   resolve(this.flicks);
    //   }, 100); });
  }

  private handleError(error: any): Promise<any> {
    this.logger.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  getFlick(index: number): Promise<Flick> {
    return new Promise(resolve => {
      this.logger.log('getFlick: ', index, this.flicks[index]);
      resolve(this.flicks[index]);
    });
  }
}
