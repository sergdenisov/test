import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlicksListComponent } from './flicks-list.component';

describe('FlicksListComponent', () => {
  let component: FlicksListComponent;
  let fixture: ComponentFixture<FlicksListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlicksListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlicksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
