import { Component, OnInit } from '@angular/core';
import {FlicksService} from '../flicks.service';
import {Flick} from '../flick';
import {LoggerService} from "../utils/logger.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-flicks-list',
  templateUrl: './flicks-list.component.html',
  styleUrls: ['./flicks-list.component.css']
})
export class FlicksListComponent implements OnInit {
  flicks: Flick[];

  constructor(public flicksService: FlicksService, private router: Router, private logger:LoggerService) {}

  ngOnInit() {
    //this.getFlicks('');
  }

  getFlicks(tags: String) {
    //this.flicksService.getFlicks("").then(flicks => {this.flicks = flicks; this.logger.log('List - flicks got:', flicks);});
  }

  onFlickClicked(flickIndex: number) {
    this.logger.log('List onFlickClicked - index ', flickIndex);
    this.router.navigate(['/full', flickIndex, 'list']);
  }
}
