import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {JsonpModule} from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing.module';

import {LoggerService} from './utils/logger.service';

import { HeadSearchComponent } from './head-search/head-search.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { FooterComponent } from './footer/footer.component';
import { FlicksListComponent } from './flicks-list/flicks-list.component';
import { FlicksGalleryComponent } from './flicks-gallery/flicks-gallery.component';
import {FlicksService} from './flicks.service';
import { FlickFullComponent } from './flick-full/flick-full.component';

@NgModule({
  declarations: [
    AppComponent,
    HeadSearchComponent,
    MainMenuComponent,
    FooterComponent,
    FlicksListComponent,
    FlicksGalleryComponent,
    FlickFullComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    AppRoutingModule
  ],
  providers: [LoggerService, FlicksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
