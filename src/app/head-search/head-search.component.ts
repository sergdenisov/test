import { Component, OnInit } from '@angular/core';

import {LoggerService} from "../utils/logger.service";
import {FlicksService} from '../flicks.service';

@Component({
  selector: 'app-head-search',
  templateUrl: './head-search.component.html',
  styleUrls: ['./head-search.component.css']
})
export class HeadSearchComponent implements OnInit {

  constructor(private flicksService: FlicksService, private logger:LoggerService) {}

  refresh(tags: string, force:boolean): void {
    this.logger.log('tags input refresh: ', tags);
    this.flicksService.updateTags(tags, force);
  }

  ngOnInit() {
    this.flicksService.updateTags('', true);
  }

}
